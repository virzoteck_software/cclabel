# -*- coding: utf-8 -*-

{
    'name': 'Get Customer Address Labels',
    'version': '1.0',
    'author': 'Virzoteck software and solution',
    'category': 'Base',
    'description': """""",
    'website':'', 
    'depends': ['base','sale'],
    'data': [
        'report/label.xml',
        'wizard/select_cus.xml'
    ],

    'installable': True,
    'auto_install': False,
    'images': ['static/description/background.png',], 
    "price":20,  
    "currency": "EUR",
    'license': 'LGPL-3'
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
