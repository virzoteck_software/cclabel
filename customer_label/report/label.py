# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models
from odoo.tools import float_round
#https://www.surekhatech.com/blog/-/blogs/generate-report-from-custom-wizard-in-odoo10?p_p_auth=g4suJ7df
#class custmer_address_label_report(models.AbstractModel):
#    _name = 'report.customer_label.custmer_address_label_report'
#
#    @api.model
#    def render_html(self, docids, data=None):
#        print'kkk===================',data['form']['real']
#        self.model = self.env.context.get('active_model')
#        docs = self.env[self.model].browse(self.env.context.get('active_id'))
#        print'----"""""""""""""""',self.env[self.model].browse(data['form']['real'])
##        docs = self.env[self.model].browse(data['form']['real'])
#
#        sales_records = []
#
#        
#        docargs = {
##           'doc_ids': self.ids,
#           'doc_model': self.model,
#           'docs': docs,
#           'selected_partner':self.env[self.model].browse(data['form']['real'])
##           'time': time,
#    #       'orders': sales_records
#        }
#        return self.env['report'].render('customer_label.custmer_address_label_report', docargs)  
class report_label(models.AbstractModel):
    _name = 'report.customer_label.sticker_report_viewz'

    @api.model
    def render_html(self, docids, data=None):
        data = data if data is not None else {}
        products = self.env['res.partner'].browse(data.get('ids', data.get('active_ids')))
        docargs = {
            'doc_ids': data.get('ids', data.get('active_ids')),
            'doc_model': 'res.partner',
            'docs': products,
            'data': dict(
                data,
                pricelist=products,
            ),
        }
        return self.env['report'].render('customer_label.sticker_report_viewz', docargs)

  